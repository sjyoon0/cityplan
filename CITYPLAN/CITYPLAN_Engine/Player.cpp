#include "pch.h"
#include "Player.h"
#include "Mesh.h"
#include "Resources.h"
#include "MeshRenderer.h"
#include "Component.h"

Player::Player()
{
}

Player::Player(PLAYER_TYPE type)
{
    _playerType = type;
}

Player::~Player()
{
}

void Player::AddWeapon(const shared_ptr<GameObject> weapon)
{
    _weapons->push_back(weapon);
}

void Player::SetMainWeapon(int index)
{
    _mainWeaponIndex = index;
}

shared_ptr<GameObject> Player::GetWeaponFromName(const wstring& name)
{
    for(int i =0; i< _weapons->size();i++)
    {
        if(_weapons->at(i)->GetName()== name)
        {
            return _weapons->at(i); 
        }
    }
    return 0;
}

shared_ptr<GameObject> Player::GetWeaponFromIndex(int index)
{
    return _weapons->at(index);
}

void Player::CreateHandIndex()
{
    if (_playerType == PLAYER_TYPE::POLICE)
    {
        _rightHandIndex = 30;
        _leftHandIndex = 10;
    }
}
