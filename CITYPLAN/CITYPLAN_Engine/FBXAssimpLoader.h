#pragma once
#include "tinyxml2.h"

struct AssimpMaterialInfo
{
	Vec4			diffuse;
	Vec4			ambient;
	Vec4			specular;

	wstring			name;
	wstring			diffuseTexName;
	wstring			normalTexName;
	wstring			specularTexName;
};


struct AssimpMeshInfo
{
	wstring								name;
	vector<Vertex>						vertices;
	vector<uint32>						indices;
	wstring								materialName;

};



class FBXAssimpLoader
{
public:
	FBXAssimpLoader();
	~FBXAssimpLoader();

	void LoadFbx(const wstring& path);

	int32 GetMeshCount() { return static_cast<int32>(_meshes.size()); }
	const AssimpMeshInfo& GetMesh(int32 idx) { return _meshes[idx]; }
	vector<shared_ptr<AssimpMaterialInfo>> GetMaterials() { return _materials; }


private:
	void Import(const wstring& path);
	
	/////////////////////////////////////////////////////
	void LoadMaterials(const wstring& path);
	AssimpMeshInfo LoadMesh(aiMesh* mesh);
	void ProcessNode(aiNode* node);

	void LoadMaterialsFromFile(const wstring& path);
	void LoadMeshFromFile(const wstring& path);
	
	
	

	
	
	/////////////////////////////////////////////////////
	void SaveMaterialData(const wstring& path, bool isOverwrite = true);
	void SaveMeshData(const wstring& path, bool isOverwrite = true);




	void CreateTextures();
	void CreateMaterials();


	void checkIndices();

private:
	Assimp::Importer* _importer = nullptr;
	const aiScene* _scene = nullptr;


	vector<AssimpMeshInfo> _meshes;
	vector<shared_ptr<AssimpMaterialInfo>> _materials;


	wstring _textureDirectory  = L"";
	wstring _materialDirectory = L"";
	wstring _meshDirectory = L"";

};

