#pragma once
class FileWriter
{
public:
	FileWriter();
	~FileWriter();

	void Open(wstring filePath, UINT openOption = CREATE_ALWAYS);
	void Close();

	void Bool(bool data);
	void Word(WORD data);
	void Int(int data);
	void Int32(int32 data);
	void Int64(int64 data);
	
	void UInt(UINT data);

	void UInt32(uint32 data);
	void UInt64(uint64 data);

	void Float(float data);
	void Double(double data);

	void Vector2(const Vec2& data);
	void Vector3(const Vec3& data);
	void Vector4(const Vec4& data);

	void BinaryMatrix(const Matrix& data);

	void String(const string& data);
	void Byte(void* data, UINT dataSize);

protected:
	HANDLE fileHandle;
	DWORD size;
};

//////////////////////////////////////////////////////////////////////////

class FileReader
{
public:
	FileReader();
	~FileReader();

	void Open(wstring filePath);
	void Close();

	bool Bool();
	WORD Word();
	int Int();
	int32 Int32();
	int64 Int64();


	UINT UInt();
	uint32 UInt32();
	uint64 UInt64();

	float Float();
	double Double();

	Vec2 Vector2();
	Vec3 Vector3();
	Vec4 Vector4();

	string String();
	void Byte(void** data, UINT dataSize);

protected:
	HANDLE fileHandle;
	DWORD size;
};