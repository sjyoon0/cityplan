#include "pch.h"
#include "OrientedBoxCollider.h"
#include "GameObject.h"
#include "Transform.h"

OrientedBoxCollider::OrientedBoxCollider():BaseCollider(ColliderType::OBB)
{
}

OrientedBoxCollider::~OrientedBoxCollider()
{
}

void OrientedBoxCollider::FinalUpdate()
{
	

	_box.pos = GetGameObject()->GetTransform()->GetWorldPosition();

	GetGameObject()->GetTransform()->GetLook().Normalize(_box.axisX);
	GetGameObject()->GetTransform()->GetUp().Normalize(_box.axisY);
	GetGameObject()->GetTransform()->GetRight().Normalize(_box.axisZ);

	Vec3 scale = GetGameObject()->GetTransform()->GetLocalScale();
	_box.extents = scale;




}

bool OrientedBoxCollider::Intersects(OBB other)
{
	return Colision(this->_box, other);
}

bool OrientedBoxCollider::SeperateingPlane(const Vec3& position, const Vec3& direction, const OBB& box1, const OBB& box2)
{

	float v1 = fabsf(position.Dot(direction));
	
	float v2 = 0.0f;
	
	v2 += fabsf((box1.axisX * box1.extents.x).Dot(direction));
	v2 += fabsf((box1.axisY * box1.extents.y).Dot(direction));
	v2 += fabsf((box1.axisZ * box1.extents.z).Dot(direction));
	v2 += fabsf((box2.axisX * box2.extents.x).Dot(direction));
	v2 += fabsf((box2.axisY * box2.extents.y).Dot(direction));
	v2 += fabsf((box2.axisZ * box2.extents.z).Dot(direction));

	return v1 > v2;
}

bool OrientedBoxCollider::Colision(OBB& box1, OBB& box2)
{

	Vec3 pos = box2.pos - box1.pos;
	if (SeperateingPlane(pos, box1.axisX, box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box1.axisY, box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box1.axisZ, box1, box2) == true)
		return false;

	if (SeperateingPlane(pos, box2.axisX, box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box2.axisY, box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box2.axisZ, box1, box2) == true)
		return false;

	if (SeperateingPlane(pos, box1.axisX.Cross(box2.axisX), box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box1.axisX.Cross(box2.axisY), box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box1.axisX.Cross(box2.axisZ), box1, box2) == true)
		return false;

	if (SeperateingPlane(pos, box1.axisY.Cross(box2.axisX), box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box1.axisY.Cross(box2.axisY), box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box1.axisY.Cross(box2.axisZ), box1, box2) == true)
		return false;

	if (SeperateingPlane(pos, box1.axisZ.Cross(box2.axisX), box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box1.axisZ.Cross(box2.axisY), box1, box2) == true)
		return false;
	if (SeperateingPlane(pos, box1.axisZ.Cross(box2.axisZ), box1, box2) == true)
		return false;
	
	return true;


}

bool OrientedBoxCollider::Intersects(Vec4 rayOrigin, Vec4 rayDir, OUT float& distance)
{
	return false;
}


