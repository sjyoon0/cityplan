#include "pch.h"
#include "Animator.h"
#include "Timer.h"
#include "Resources.h"
#include "Material.h"
#include "Mesh.h"
#include "MeshRenderer.h"
#include "StructuredBuffer.h"

Animator::Animator() : Component(COMPONENT_TYPE::ANIMATOR)
{
	_computeMaterial = GET_SINGLE(Resources)->Get<Material>(L"ComputeAnimation");
	_boneFinalMatrix = make_shared<StructuredBuffer>();
}

Animator::~Animator()
{
}

void Animator::FinalUpdate()
{
	_tween.curr.updateTime += DELTA_TIME;

	const AnimClipInfo& animClip = _animClips->at(_tween.curr.index);
	if (_tween.curr.updateTime >= animClip.duration)
		_tween.curr.updateTime = 0.f;

	const int32 ratio = static_cast<int32>(animClip.frameCount / animClip.duration);
	_tween.curr.currFrame = static_cast<int32>(_tween.curr.updateTime * ratio);
	_tween.curr.currFrame = min(_tween.curr.currFrame, animClip.frameCount - 1);
	_tween.curr.nextFrame = min(_tween.curr.currFrame + 1, animClip.frameCount - 1);
	_tween.curr.frameRatio = static_cast<float>(_tween.curr.currFrame - _tween.curr.currFrame);


	if (_tween.next.index > -1)
	{
		_tween.tweenTime += DELTA_TIME;
		const AnimClipInfo& nextClip = _animClips->at(_tween.next.index);

		
		if (_tween.tweenTime >= 1.0f)
		{
			_tween.curr = _tween.next;
			_tween.next.index = -1;
			_tween.next.frameRatio = 0.0f;
			_tween.next.currFrame = 0;
			_tween.next.nextFrame = 0;
			_tween.next.updateTime = 0.0f;
			
			_tween.tweenTime = 0.0f;
		}
		else
		{
			_tween.next.updateTime += DELTA_TIME;

			if (_tween.next.updateTime >= nextClip.duration)
				_tween.next.updateTime = 0.f;

			const int32 ratio = static_cast<int32>(nextClip.frameCount / nextClip.duration);
			_tween.next.currFrame = static_cast<int32>(_tween.next.updateTime * ratio);
			_tween.next.currFrame = min(_tween.next.currFrame, nextClip.frameCount - 1);
			_tween.next.nextFrame = min(_tween.next.currFrame + 1, nextClip.frameCount - 1);
			_tween.next.frameRatio = static_cast<float>(_tween.next.currFrame - _tween.next.currFrame);

		}
	}
}

void Animator::SetAnimClip(const vector<AnimClipInfo>* animClips)
{
	_animClips = animClips;
}

void Animator::PushData()
{
	uint32 boneCount = static_cast<uint32>(_bones->size());
	if (_boneFinalMatrix->GetElementCount() < boneCount)
		_boneFinalMatrix->Init(sizeof(Matrix), boneCount);

	// Compute Shader
	shared_ptr<Mesh> mesh = GetGameObject()->GetMeshRenderer()->GetMesh();
	mesh->GetBoneFrameDataBuffer(_tween.curr.index)->PushComputeSRVData(SRV_REGISTER::t8);
	
	if(_tween.next.index > -1)
		mesh->GetBoneFrameDataBuffer(_tween.next.index)->PushComputeSRVData(SRV_REGISTER::t6);
	else
		mesh->GetBoneFrameDataBuffer(_tween.curr.index)->PushComputeSRVData(SRV_REGISTER::t6);

	mesh->GetBoneOffsetBuffer()->PushComputeSRVData(SRV_REGISTER::t9);

	_boneFinalMatrix->PushComputeUAVData(UAV_REGISTER::u0);

	_computeMaterial->SetInt(0, boneCount);
	_computeMaterial->SetFloat(0, _tween.tweenTime);
	_computeMaterial->SetVec4(0, 
		Vec4(static_cast<float>(_tween.curr.index),
			static_cast<float>(_tween.curr.currFrame),
			static_cast<float>(_tween.curr.nextFrame),
			_tween.curr.frameRatio));
	_computeMaterial->SetVec4(1, 
		Vec4(static_cast<float>(_tween.next.index),
			static_cast<float>(_tween.next.currFrame),
			static_cast<float>(_tween.next.nextFrame),
			_tween.next.frameRatio));


	uint32 groupCount = (boneCount / 256) + 1;
	_computeMaterial->Dispatch(groupCount, 1, 1);

	// Graphics Shader
	_boneFinalMatrix->PushGraphicsData(SRV_REGISTER::t7);
}

void Animator::Play(uint32 idx)
{	
	assert(idx < _animClips->size());
	
	if (_tween.curr.index != idx)
	{
		_tween.next.index = idx;
	}


}