#include "pch.h"
#include "TestSoldier.h"
#include "Transform.h"
#include "Input.h"
#include "Timer.h"
#include "Animator.h"
#include "GameObject.h"
#include "SceneManager.h"
#include "Mesh.h"
#include "MeshRenderer.h"
#include "OrientedBoxCollider.h"
#include "Scene.h"


TestSoldier::TestSoldier()
{
}

TestSoldier::~TestSoldier()
{
}

void TestSoldier::Awake()
{


}

void TestSoldier::Update()
{

	Vec3 position = GetTransform()->GetLocalPosition();

	if (INPUT->GetButtonDown(KEY_TYPE::SPACE))
	{
		Vec3 jumppostion = position;
		isJump = true;
	}

	if (isJump)
	{
		_jumpTime -= DELTA_TIME;

		if (_jumpTime > 2.8 && _jumpTime <= 3.8f)
		{

			GetAnimator()->Play(ANIMNUM::jump_up);
		}
		else if (_jumpTime > 1.5f && _jumpTime <= 2.8f)
		{
			position += GetTransform()->GetUp() * _jump * DELTA_TIME;
			GetAnimator()->Play(ANIMNUM::jump_loop);
		}
		else if (_jumpTime > 0.f && _jumpTime <= 1.5f)
		{
			position -= GetTransform()->GetUp() * _jump * DELTA_TIME;
			GetAnimator()->Play(ANIMNUM::jump_loop);
		}
		else if (_jumpTime <= 0.f)
		{
			GetAnimator()->Play(ANIMNUM::jump_down);
			_jumpTime = 3.0f;
			isJump = false;
			jumppostion = position;
		}
	}


	if (INPUT->GetButtonDown(KEY_TYPE::LSHIFT))
		isRun = true;

	if (INPUT->GetButtonUp(KEY_TYPE::LSHIFT))
		isRun = false;

	if (!isJump)
	{
		if (isRun)
		{
			if (INPUT->GetButton(KEY_TYPE::W))
			{
				position -= GetTransform()->GetLook() * _speed * DELTA_TIME;
				GetAnimator()->Play(ANIMNUM::run_forward_stand);
			}

			if (INPUT->GetButton(KEY_TYPE::S))
			{
				position += GetTransform()->GetLook() * _speed * DELTA_TIME;
				GetAnimator()->Play(ANIMNUM::run_backward_stand);
			}

			if (INPUT->GetButton(KEY_TYPE::A))
			{
				position += GetTransform()->GetRight() * _speed * DELTA_TIME;
				GetAnimator()->Play(ANIMNUM::run_left_stand);
			}
			if (INPUT->GetButton(KEY_TYPE::D))
			{
				position -= GetTransform()->GetRight() * _speed * DELTA_TIME;
				GetAnimator()->Play(ANIMNUM::run_right_stand);
			}
		}
		else
		{
			if (INPUT->GetButton(KEY_TYPE::W))
			{
				position -= GetTransform()->GetLook() * _speed * DELTA_TIME * 0.5;
				GetAnimator()->Play(ANIMNUM::walking_forward_stand);
			}

			if (INPUT->GetButton(KEY_TYPE::S))
			{
				position += GetTransform()->GetLook() * _speed * DELTA_TIME * 0.5;
				GetAnimator()->Play(ANIMNUM::walking_backward_stand);
			}

			if (INPUT->GetButton(KEY_TYPE::A))
			{
				position += GetTransform()->GetRight() * _speed * DELTA_TIME * 0.5;
				GetAnimator()->Play(ANIMNUM::walking_left_stand);
			}
			if (INPUT->GetButton(KEY_TYPE::D))
			{
				position -= GetTransform()->GetRight() * _speed * DELTA_TIME * 0.5;
				GetAnimator()->Play(ANIMNUM::walking_right_stand);
			}
		}



		if (!isRun)
		{	// stand
			if (_pos == 0)
			{
				if (INPUT->GetButtonDown(KEY_TYPE::C))
				{
					GetAnimator()->Play(ANIMNUM::idle_aim_crouch);
					_pos = 1;
				}
				if (INPUT->GetButtonDown(KEY_TYPE::V))
				{
					GetAnimator()->Play(ANIMNUM::idle_aim_prone);
					_pos = 2;
				}
				if (INPUT->GetButtonDown(KEY_TYPE::R))
					GetAnimator()->Play(ANIMNUM::reload_stand);

				if (INPUT->GetButtonDown(KEY_TYPE::LBUTTON))
					GetAnimator()->Play(ANIMNUM::shoot_stand);

				if (INPUT->GetButtonDown(KEY_TYPE::RBUTTON))
					GetAnimator()->Play(ANIMNUM::throw_grenade_stand);
			}
			// crouch
			else if (_pos == 1)
			{
				if (INPUT->GetButtonDown(KEY_TYPE::C))
				{
					GetAnimator()->Play(ANIMNUM::idle_aim_stand);
					_pos = 0;
				}
				if (INPUT->GetButtonDown(KEY_TYPE::V))
				{
					GetAnimator()->Play(ANIMNUM::idle_aim_prone);
					_pos = 2;
				}
				if (INPUT->GetButtonDown(KEY_TYPE::R))
					GetAnimator()->Play(ANIMNUM::reload_crouch);

				if (INPUT->GetButtonDown(KEY_TYPE::LBUTTON))
					GetAnimator()->Play(ANIMNUM::shoot_crouch);

				if (INPUT->GetButtonDown(KEY_TYPE::RBUTTON))
					GetAnimator()->Play(ANIMNUM::throw_grenade_crouch);


			}
			// prone
			else if (_pos == 2)
			{
				if (INPUT->GetButtonDown(KEY_TYPE::C))
				{
					GetAnimator()->Play(ANIMNUM::idle_aim_crouch);
					_pos = 1;
				}
				if (INPUT->GetButtonDown(KEY_TYPE::V))
				{
					GetAnimator()->Play(ANIMNUM::idle_aim_stand);
					_pos = 0;
				}

				if (INPUT->GetButtonDown(KEY_TYPE::R))
					GetAnimator()->Play(ANIMNUM::reload_prone);

				if (INPUT->GetButtonDown(KEY_TYPE::LBUTTON))
					GetAnimator()->Play(ANIMNUM::shoot_prone);

				if (INPUT->GetButtonDown(KEY_TYPE::RBUTTON))
					GetAnimator()->Play(ANIMNUM::throw_grenade_prone);
			}


		}
	}


	if (INPUT->GetButtonDown(KEY_TYPE::F1))
		position = Vec3(-12040.f, 10.0f, -6389.f);



	GetTransform()->SetLocalPosition(position);


}


void TestSoldier::Start()
{

	GetAnimator()->Play(ANIMNUM::idle_aim_stand);
}

void TestSoldier::LateUpdate()
{
	int count = 0;
	auto& gameObjects = GET_SINGLE(SceneManager)->GetActiveScene()->GetGameObjects();

	for (auto& gameObject : gameObjects)
	{
		if (gameObject->GetName() == L"MapOBB")
		{
			if (gameObject->GetCollider()->GetColliderType() == ColliderType::OBB)
			{
				if (GetGameObject()->GetCollider()->Intersects(gameObject->GetCollider()->GetOBB()))
				{

					_speed = 0.f;

				}
				else
					_speed = 500.f;
			}
		}
	}

}