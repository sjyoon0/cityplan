#pragma once

class Scene;

enum
{
	MAX_LAYER = 32
};

struct Data
{
	Vec3 pos;
	Vec3 rotation;
	Vec3 scale;
};



class SceneManager
{
	DECLARE_SINGLE(SceneManager);

public:
	void Update();
	void Render();
	void LoadScene(wstring sceneName);

	void SetLayerName(uint8 index, const wstring& name);
	const wstring& IndexToLayerName(uint8 index) { return _layerNames[index]; }
	uint8 LayerNameToIndex(const wstring& name);

	shared_ptr<class GameObject> Pick(int32 screenX, int32 screenY);

public:
	shared_ptr<Scene> GetActiveScene() { return _activeScene; }
	const Matrix& GetPlayerData();
private:
	shared_ptr<Scene> LoadTestScene();
    void CreateFromMapData(const wstring& path, shared_ptr<Scene> scene);
	const vector<Data>* LoadFromOBBData(const wstring& path);
	

private:
	shared_ptr<Scene> _activeScene;

	array<wstring, MAX_LAYER> _layerNames;
	map<wstring, uint8> _layerIndex;
};