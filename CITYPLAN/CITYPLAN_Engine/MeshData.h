#pragma once
#include "Object.h"

class Mesh;
class Material;
class GameObject;

struct MeshRenderInfo
{
	shared_ptr<Mesh>				mesh;
	vector<shared_ptr<Material>>	materials;
};

class MeshData : public Object
{
public:
	MeshData();
	virtual ~MeshData();

public:
	static shared_ptr<MeshData> LoadFromFBX(const wstring& path);
	
	void AddMeshRender(MeshRenderInfo info) { _meshRenders.push_back(info); }

	shared_ptr<Mesh> GetMesh(int index) { return _meshRenders.at(index).mesh; }

	virtual void Load(const wstring& path);
	virtual void Save(const wstring& path);

	vector<shared_ptr<GameObject>> Instantiate();

private:
	vector<MeshRenderInfo> _meshRenders;
};
