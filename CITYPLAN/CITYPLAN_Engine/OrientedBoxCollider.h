#pragma once
#include "BaseCollider.h"


class OrientedBoxCollider : public BaseCollider
{
public:
	OrientedBoxCollider();
	virtual ~OrientedBoxCollider();

	virtual void FinalUpdate() override;
	virtual bool Intersects(OBB other);
	virtual bool Intersects(Vec4 rayOrigin, Vec4 rayDir, OUT float& distance);

	bool SeperateingPlane(const Vec3& position, const Vec3& direction, const OBB& box1, const OBB& box2);
	bool Colision(OBB& box1, OBB& box2);

	void SetRadius(Vec3 extents) { _extents = extents; }
	void SetCenter(Vec3 center) { _center = center; }

private:
	// Local ����
	Vec3		_extents = Vec3(1.f,1.f,1.f);
	Vec3		_center = Vec3(0, 0, 0);

};

