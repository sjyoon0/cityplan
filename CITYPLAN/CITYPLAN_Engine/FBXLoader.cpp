#include "pch.h"
#include "FBXLoader.h"
#include "Mesh.h"
#include "Resources.h"
#include "Shader.h"
#include "Material.h"
#include "FileReadWrite.h"

FBXLoader::FBXLoader()
{

}

FBXLoader::~FBXLoader()
{
	if (_scene)
		_scene->Destroy();

	if (_manager)
		_manager->Destroy();


}

void FBXLoader::LoadFbx(const wstring& path)
{

	if (fs::path(path).parent_path().filename().wstring() == L"Maps")
		_isInMapDirectory = true;

	wstring filePath = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".mesh";
	if (fs::exists(filePath))
		Load(path);
	else
	{

		// 파일 데이터 로드
		Import(path);


		// Animation	
		LoadBones(_scene->GetRootNode());
		LoadAnimationInfo();


		// 로드된 데이터 파싱 (Mesh/Material/Skin)
		ParseNode(_scene->GetRootNode());

		if (fs::path(path).parent_path().extension().wstring() != L".animdir")
		{
			LoadAnimationFromDir(path);
			Save(path);
		}
	}


	// 우리 구조에 맞게 Texture / Material 생성
	CreateTextures();
	CreateMaterials();


	checkIndice();
}

void FBXLoader::Save(const wstring& path)
{
	// complete
	if (!_meshes.empty())
		SaveMesh(path);
	// complete
	if (!_bones.empty())
		SaveBone(path);
	// 
	if (!_animClips.empty())
		SaveAnimation(path);

}

void FBXLoader::Load(const wstring& path)
{
	if (_isInMapDirectory)
		_resourceDirectory = L"..\\Resources\\Texture\\MapTexture";
	else
		_resourceDirectory = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".fbm";

	wstring filePath = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".mesh";

	LoadMeshFromFile(filePath);

	filePath = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".bones";
	if (fs::exists(filePath))
		LoadBoneFromFile(filePath);

	filePath = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".animations";
	if (fs::exists(filePath))
		LoadAnimFromFile(filePath);


}

void FBXLoader::Import(const wstring& path)
{
	// FBX SDK 관리자 객체 생성
	_manager = FbxManager::Create();

	// IOSettings 객체 생성 및 설정
	FbxIOSettings* settings = FbxIOSettings::Create(_manager, IOSROOT);
	_manager->SetIOSettings(settings);

	// FbxImporter 객체 생성
	_scene = FbxScene::Create(_manager, "");

	// 나중에 Texture 경로 계산할 때 쓸 것
	if (_isInMapDirectory)
		_resourceDirectory = L"..\\Resources\\Texture\\MapTexture";
	else
		_resourceDirectory = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".fbm";

	_fileName = fs::path(path).filename().stem().wstring();

	_animDirectory = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".animdir";

	_importer = FbxImporter::Create(_manager, "");

	string strPath = ws2s(path);
	_importer->Initialize(strPath.c_str(), -1, _manager->GetIOSettings());

	_importer->Import(_scene);

	_scene->GetGlobalSettings().SetAxisSystem(FbxAxisSystem::DirectX);

	// 씬 내에서 삼각형화 할 수 있는 모든 노드를 삼각형화 시킨다.
	FbxGeometryConverter geometryConverter(_manager);
	geometryConverter.Triangulate(_scene, true);

	_importer->Destroy();
}

void FBXLoader::ParseNode(FbxNode* node)
{
	FbxNodeAttribute* attribute = node->GetNodeAttribute();

	if (attribute)
	{
		switch (attribute->GetAttributeType())
		{
		case FbxNodeAttribute::eMesh:
			LoadMesh(node->GetMesh());
			break;
		}
	}

	// Material 로드
	const uint32 materialCount = node->GetMaterialCount();

	for (uint32 i = 0; i < materialCount; ++i)
	{
		FbxSurfaceMaterial* surfaceMaterial = node->GetMaterial(i);
		
		LoadMaterial(surfaceMaterial);
	}
	
	// Tree 구조 재귀 호출
	const int32 childCount = node->GetChildCount();
	for (int32 i = 0; i < childCount; ++i)
		ParseNode(node->GetChild(i));
}

void FBXLoader::LoadMesh(FbxMesh* mesh)
{
	_meshes.push_back(FbxMeshInfo());
	FbxMeshInfo& meshInfo = _meshes.back();

	meshInfo.name = s2ws(mesh->GetName());

	const int32 vertexCount = mesh->GetControlPointsCount();
	meshInfo.vertices.resize(vertexCount);
	meshInfo.boneWeights.resize(vertexCount);

	// Position
	FbxVector4* controlPoints = mesh->GetControlPoints();
	for (int32 i = 0; i < vertexCount; ++i)
	{
		meshInfo.vertices[i].pos.x = static_cast<float>(controlPoints[i].mData[0]);
		meshInfo.vertices[i].pos.y = static_cast<float>(controlPoints[i].mData[1]);
		meshInfo.vertices[i].pos.z = static_cast<float>(controlPoints[i].mData[2]);
	}

	const int32 materialCount = mesh->GetNode()->GetMaterialCount();
	meshInfo.indices.resize(materialCount);

	FbxGeometryElementMaterial* geometryElementMaterial = mesh->GetElementMaterial();

	const int32 polygonSize = mesh->GetPolygonSize(0);
	assert(polygonSize == 3);

	uint32 arrIdx[3];
	uint32 vertexCounter = 0; // 정점의 개수

	const int32 triCount = mesh->GetPolygonCount(); // 메쉬의 삼각형 개수를 가져온다
	for (int32 i = 0; i < triCount; i++) // 삼각형의 개수
	{
		for (int32 j = 0; j < 3; j++) // 삼각형은 세 개의 정점으로 구성
		{
			int32 controlPointIndex = mesh->GetPolygonVertex(i, j); // 제어점의 인덱스 추출
			arrIdx[j] = controlPointIndex;

			GetNormal(mesh, &meshInfo, controlPointIndex, vertexCounter);
			GetTangent(mesh, &meshInfo, controlPointIndex, vertexCounter);
			GetUV(mesh, &meshInfo, controlPointIndex, mesh->GetTextureUVIndex(i, j));

			vertexCounter++;
		}

		const uint32 subsetIdx = geometryElementMaterial->GetIndexArray().GetAt(i);
		meshInfo.indices[subsetIdx].push_back(arrIdx[0]);
		meshInfo.indices[subsetIdx].push_back(arrIdx[1]);
		meshInfo.indices[subsetIdx].push_back(arrIdx[2]);
	}


	LoadAnimationData(mesh, &meshInfo);
}

void FBXLoader::LoadMaterial(FbxSurfaceMaterial* surfaceMaterial)
{
	FbxMaterialInfo material{};

	material.name = s2ws(surfaceMaterial->GetName());

	material.diffuse = GetMaterialData(surfaceMaterial, FbxSurfaceMaterial::sDiffuse, FbxSurfaceMaterial::sDiffuseFactor);
	material.ambient = GetMaterialData(surfaceMaterial, FbxSurfaceMaterial::sAmbient, FbxSurfaceMaterial::sAmbientFactor);
	material.specular = GetMaterialData(surfaceMaterial, FbxSurfaceMaterial::sSpecular, FbxSurfaceMaterial::sSpecularFactor);

	material.diffuseTexName = GetTextureRelativeName(surfaceMaterial, FbxSurfaceMaterial::sDiffuse);
	material.normalTexName = GetTextureRelativeName(surfaceMaterial, FbxSurfaceMaterial::sNormalMap);
	material.specularTexName = GetTextureRelativeName(surfaceMaterial, FbxSurfaceMaterial::sSpecular);




	_meshes.back().materials.push_back(material);
}

void FBXLoader::GetNormal(FbxMesh* mesh, FbxMeshInfo* container, int32 idx, int32 vertexCounter)
{
	if (mesh->GetElementNormalCount() == 0)
		return;

	FbxGeometryElementNormal* normal = mesh->GetElementNormal();
	uint32 normalIdx = 0;

	if (normal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
	{
		if (normal->GetReferenceMode() == FbxGeometryElement::eDirect)
			normalIdx = vertexCounter;
		else
			normalIdx = normal->GetIndexArray().GetAt(vertexCounter);
	}
	else if (normal->GetMappingMode() == FbxGeometryElement::eByControlPoint)
	{
		if (normal->GetReferenceMode() == FbxGeometryElement::eDirect)
			normalIdx = idx;
		else
			normalIdx = normal->GetIndexArray().GetAt(idx);
	}

	FbxVector4 vec = normal->GetDirectArray().GetAt(normalIdx);
	container->vertices[idx].normal.x = static_cast<float>(vec.mData[0]);
	container->vertices[idx].normal.y = static_cast<float>(vec.mData[1]);
	container->vertices[idx].normal.z = static_cast<float>(vec.mData[2]);
}

void FBXLoader::GetTangent(FbxMesh* mesh, FbxMeshInfo* meshInfo, int32 idx, int32 vertexCounter)
{
	if (mesh->GetElementTangentCount() == 0)
	{
		meshInfo->vertices[idx].tangent.x = 1.f;
		meshInfo->vertices[idx].tangent.y = 0.f;
		meshInfo->vertices[idx].tangent.z = 0.f;
		return;
	}

	FbxGeometryElementTangent* tangent = mesh->GetElementTangent();
	uint32 tangentIdx = 0;

	if (tangent->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
	{
		if (tangent->GetReferenceMode() == FbxGeometryElement::eDirect)
			tangentIdx = vertexCounter;
		else
			tangentIdx = tangent->GetIndexArray().GetAt(vertexCounter);
	}
	else if (tangent->GetMappingMode() == FbxGeometryElement::eByControlPoint)
	{
		if (tangent->GetReferenceMode() == FbxGeometryElement::eDirect)
			tangentIdx = idx;
		else
			tangentIdx = tangent->GetIndexArray().GetAt(idx);
	}

	FbxVector4 vec = tangent->GetDirectArray().GetAt(tangentIdx);
	meshInfo->vertices[idx].tangent.x = static_cast<float>(vec.mData[0]);
	meshInfo->vertices[idx].tangent.y = static_cast<float>(vec.mData[1]);
	meshInfo->vertices[idx].tangent.z = static_cast<float>(vec.mData[2]);
}

void FBXLoader::GetUV(FbxMesh* mesh, FbxMeshInfo* meshInfo, int32 idx, int32 uvIndex)
{
	FbxVector2 uv = mesh->GetElementUV()->GetDirectArray().GetAt(uvIndex);
	meshInfo->vertices[idx].uv.x = static_cast<float>(uv.mData[0]);
	meshInfo->vertices[idx].uv.y = 1.f - static_cast<float>(uv.mData[1]);
}

Vec4 FBXLoader::GetMaterialData(FbxSurfaceMaterial* surface, const char* materialName, const char* factorName)
{
	FbxDouble3  material;
	FbxDouble	factor = 0.f;

	FbxProperty materialProperty = surface->FindProperty(materialName);
	FbxProperty factorProperty = surface->FindProperty(factorName);


	if (materialProperty.IsValid() && factorProperty.IsValid())
	{
		material = materialProperty.Get<FbxDouble3>();
		factor = factorProperty.Get<FbxDouble>();
	}

	Vec4 ret = Vec4(
		static_cast<float>(material.mData[0] * factor),
		static_cast<float>(material.mData[1] * factor),
		static_cast<float>(material.mData[2] * factor),
		static_cast<float>(factor));

	return ret;
}

wstring FBXLoader::GetTextureRelativeName(FbxSurfaceMaterial* surface, const char* materialProperty)
{
	string name;

	FbxProperty textureProperty = surface->FindProperty(materialProperty);
	if (textureProperty.IsValid())
	{
		uint32 count = textureProperty.GetSrcObjectCount();

		if (1 <= count)
		{
			FbxFileTexture* texture = textureProperty.GetSrcObject<FbxFileTexture>(0);
			if (texture)
				name = texture->GetRelativeFileName();
		}
	}

	return s2ws(name);
}

void FBXLoader::CreateTextures()
{
	for (size_t i = 0; i < _meshes.size(); i++)
	{
		for (size_t j = 0; j < _meshes[i].materials.size(); j++)
		{
			// DiffuseTexture
			{
				wstring relativePath = _meshes[i].materials[j].diffuseTexName.c_str();
				wstring filename = fs::path(relativePath).filename();
				wstring fullPath = _resourceDirectory + L"\\" + filename;


				if (filename.empty() == false)
				{
					if (GET_SINGLE(Resources)->Get<Texture>(filename) == nullptr)
					{
						GET_SINGLE(Resources)->Load<Texture>(filename, fullPath);
						if (GET_SINGLE(Resources)->Load<Texture>(filename, fullPath)->IsAlpha())
							_isAlphaTexture = false;

					}

				}
			}

			// NormalTexture
			{
				wstring relativePath = _meshes[i].materials[j].normalTexName.c_str();
				wstring filename = fs::path(relativePath).filename();
				wstring fullPath = _resourceDirectory + L"\\" + filename;
				if (filename.empty() == false)
				{
					if (GET_SINGLE(Resources)->Get<Texture>(filename) == nullptr)
						GET_SINGLE(Resources)->Load<Texture>(filename, fullPath);

				}
			}

			// SpecularTexture
			{
				wstring relativePath = _meshes[i].materials[j].specularTexName.c_str();
				wstring filename = fs::path(relativePath).filename();
				wstring fullPath = _resourceDirectory + L"\\" + filename;
				if (filename.empty() == false)
				{
					if(GET_SINGLE(Resources)->Get<Texture>(filename) == nullptr)
						GET_SINGLE(Resources)->Load<Texture>(filename, fullPath);

				}
			}
		}
	}
}

void FBXLoader::CreateMaterials()
{
	for (size_t i = 0; i < _meshes.size(); i++)
	{
		for (size_t j = 0; j < _meshes[i].materials.size(); j++)
		{
			shared_ptr<Material> material = make_shared<Material>();
			wstring key = _meshes[i].materials[j].name;
			material->SetName(key);
			if (_isAlphaTexture)
				material->SetShader(GET_SINGLE(Resources)->Get<Shader>(L"AlphaDeferred"));
			else	
				material->SetShader(GET_SINGLE(Resources)->Get<Shader>(L"Deferred"));

			{
				wstring diffuseName = _meshes[i].materials[j].diffuseTexName.c_str();
				wstring filename = fs::path(diffuseName).filename();
				wstring key = filename;
				shared_ptr<Texture> diffuseTexture = GET_SINGLE(Resources)->Get<Texture>(key);
				
				if (diffuseTexture)
					material->SetTexture(0, diffuseTexture);
			}

			{
				wstring normalName = _meshes[i].materials[j].normalTexName.c_str();
				wstring filename = fs::path(normalName).filename();
				wstring key = filename;
				shared_ptr<Texture> normalTexture = GET_SINGLE(Resources)->Get<Texture>(key);
				if (normalTexture)
					material->SetTexture(1, normalTexture);
			}

			{
				wstring specularName = _meshes[i].materials[j].specularTexName.c_str();
				wstring filename = fs::path(specularName).filename();
				wstring key = filename;
				shared_ptr<Texture> specularTexture = GET_SINGLE(Resources)->Get<Texture>(key);
				if (specularTexture)
					material->SetTexture(2, specularTexture);
			}

			if (GET_SINGLE(Resources)->Get<Material>(material->GetName()) == nullptr)
				GET_SINGLE(Resources)->Add<Material>(material->GetName(), material);
			else
				material = GET_SINGLE(Resources)->Get<Material>(material->GetName());
		}
	}
}

void FBXLoader::LoadBones(FbxNode* node, int32 idx, int32 parentIdx)
{
	FbxNodeAttribute* attribute = node->GetNodeAttribute();

	if (attribute && attribute->GetAttributeType() == FbxNodeAttribute::eSkeleton)
	{
		shared_ptr<FbxBoneInfo> bone = make_shared<FbxBoneInfo>();
		bone->boneName = s2ws(node->GetName());
		bone->parentIndex = parentIdx;
		_bones.push_back(bone);
	}

	const int32 childCount = node->GetChildCount();
	for (int32 i = 0; i < childCount; i++)
		LoadBones(node->GetChild(i), static_cast<int32>(_bones.size()), idx);
}

void FBXLoader::LoadAnimationInfo()
{
	_scene->FillAnimStackNameArray(OUT _animNames);

	const int32 animCount = _animNames.GetCount();
	for (int32 i = 0; i < animCount; i++)
	{
		FbxAnimStack* animStack = _scene->FindMember<FbxAnimStack>(_animNames[i]->Buffer());
		if (animStack == nullptr)
			continue;

		shared_ptr<FbxAnimClipInfo> animClip = make_shared<FbxAnimClipInfo>();


		if (animCount == 1)
			animClip->name = _fileName;
		else
			animClip->name = s2ws(animStack->GetName());
		animClip->keyFrames.resize(_bones.size()); // 키프레임은 본의 개수만큼

		FbxTakeInfo* takeInfo = _scene->GetTakeInfo(animStack->GetName());
		animClip->startTime = takeInfo->mLocalTimeSpan.GetStart();
		animClip->endTime = takeInfo->mLocalTimeSpan.GetStop();
		animClip->mode = _scene->GetGlobalSettings().GetTimeMode();



		_animClips.push_back(animClip);
	}
}

void FBXLoader::LoadAnimationData(FbxMesh* mesh, FbxMeshInfo* meshInfo)
{
	const int32 skinCount = mesh->GetDeformerCount(FbxDeformer::eSkin);
	if (skinCount <= 0 || _animClips.empty())
		return;

	meshInfo->hasAnimation = true;

	for (int32 i = 0; i < skinCount; i++)
	{
		FbxSkin* fbxSkin = static_cast<FbxSkin*>(mesh->GetDeformer(i, FbxDeformer::eSkin));

		if (fbxSkin)
		{
			FbxSkin::EType type = fbxSkin->GetSkinningType();
			if (FbxSkin::eRigid == type || FbxSkin::eLinear)
			{
				const int32 clusterCount = fbxSkin->GetClusterCount();
				for (int32 j = 0; j < clusterCount; j++)
				{
					FbxCluster* cluster = fbxSkin->GetCluster(j);
					if (cluster->GetLink() == nullptr)
						continue;

					int32 boneIdx = FindBoneIndex(cluster->GetLink()->GetName());
					assert(boneIdx >= 0);

					FbxAMatrix matNodeTransform = GetTransform(mesh->GetNode());
					LoadBoneWeight(cluster, boneIdx, meshInfo);
					LoadOffsetMatrix(cluster, matNodeTransform, boneIdx, meshInfo);

					const int32 animCount = _animNames.Size();
					for (int32 k = 0; k < animCount; k++)
						LoadKeyframe(k, mesh->GetNode(), cluster, matNodeTransform, boneIdx, meshInfo);
				}
			}
		}
	}

	FillBoneWeight(mesh, meshInfo);
}

void FBXLoader::FillBoneWeight(FbxMesh* mesh, FbxMeshInfo* meshInfo)
{
	const int32 size = static_cast<int32>(meshInfo->boneWeights.size());
	for (int32 v = 0; v < size; v++)
	{
		BoneWeight& boneWeight = meshInfo->boneWeights[v];
		boneWeight.Normalize();

		float animBoneIndex[4] = {};
		float animBoneWeight[4] = {};

		const int32 weightCount = static_cast<int32>(boneWeight.boneWeights.size());
		for (int32 w = 0; w < weightCount; w++)
		{
			animBoneIndex[w] = static_cast<float>(boneWeight.boneWeights[w].first);
			animBoneWeight[w] = static_cast<float>(boneWeight.boneWeights[w].second);
		}

		memcpy(&meshInfo->vertices[v].indices, animBoneIndex, sizeof(Vec4));
		memcpy(&meshInfo->vertices[v].weights, animBoneWeight, sizeof(Vec4));
	}
}

void FBXLoader::LoadBoneWeight(FbxCluster* cluster, int32 boneIdx, FbxMeshInfo* meshInfo)
{
	const int32 indicesCount = cluster->GetControlPointIndicesCount();
	for (int32 i = 0; i < indicesCount; i++)
	{
		double weight = cluster->GetControlPointWeights()[i];
		int32 vtxIdx = cluster->GetControlPointIndices()[i];
		meshInfo->boneWeights[vtxIdx].AddWeights(boneIdx, weight);
	}
}

void FBXLoader::LoadOffsetMatrix(FbxCluster* cluster, const FbxAMatrix& matNodeTransform, int32 boneIdx, FbxMeshInfo* meshInfo)
{
	FbxAMatrix matClusterTrans;
	FbxAMatrix matClusterLinkTrans;
	// The transformation of the mesh at binding time 
	cluster->GetTransformMatrix(matClusterTrans);
	// The transformation of the cluster(joint) at binding time from joint space to world space 
	cluster->GetTransformLinkMatrix(matClusterLinkTrans);

	FbxVector4 V0 = { 1, 0, 0, 0 };
	FbxVector4 V1 = { 0, 0, 1, 0 };
	FbxVector4 V2 = { 0, 1, 0, 0 };
	FbxVector4 V3 = { 0, 0, 0, 1 };

	FbxAMatrix matReflect;
	matReflect[0] = V0;
	matReflect[1] = V1;
	matReflect[2] = V2;
	matReflect[3] = V3;

	FbxAMatrix matOffset;
	matOffset = matClusterLinkTrans.Inverse() * matClusterTrans;
	matOffset = matReflect * matOffset * matReflect;

	_bones[boneIdx]->matOffset = matOffset.Transpose();
}

void FBXLoader::LoadKeyframe(int32 animIndex, FbxNode* node, FbxCluster* cluster, const FbxAMatrix& matNodeTransform, int32 boneIdx, FbxMeshInfo* meshInfo)
{
	if (_animClips.empty())
		return;

	FbxVector4	v1 = { 1, 0, 0, 0 };
	FbxVector4	v2 = { 0, 0, 1, 0 };
	FbxVector4	v3 = { 0, 1, 0, 0 };
	FbxVector4	v4 = { 0, 0, 0, 1 };
	FbxAMatrix	matReflect;
	matReflect.mData[0] = v1;
	matReflect.mData[1] = v2;
	matReflect.mData[2] = v3;
	matReflect.mData[3] = v4;

	fbxsdk::FbxTime::EMode timeMode = _scene->GetGlobalSettings().GetTimeMode();

	// 애니메이션 골라줌
	FbxAnimStack* animStack = _scene->FindMember<FbxAnimStack>(_animNames[animIndex]->Buffer());
	_scene->SetCurrentAnimationStack(OUT animStack);

	FbxLongLong startFrame = _animClips[animIndex]->startTime.GetFrameCount(timeMode);
	FbxLongLong endFrame = _animClips[animIndex]->endTime.GetFrameCount(timeMode);

	for (FbxLongLong frame = startFrame; frame < endFrame; frame++)
	{
		FbxKeyFrameInfo keyFrameInfo = {};
		fbxsdk::FbxTime fbxTime = 0;

		fbxTime.SetFrame(frame, timeMode);

		FbxAMatrix matFromNode = node->EvaluateGlobalTransform(fbxTime);
		FbxAMatrix matTransform = matFromNode.Inverse() * cluster->GetLink()->EvaluateGlobalTransform(fbxTime);
		matTransform = matReflect * matTransform * matReflect;

		keyFrameInfo.time = fbxTime.GetSecondDouble();
		keyFrameInfo.matTransform = matTransform;

		_animClips[animIndex]->keyFrames[boneIdx].push_back(keyFrameInfo);
	}
}

int32 FBXLoader::FindBoneIndex(string name)
{
	wstring boneName = wstring(name.begin(), name.end());

	for (UINT i = 0; i < _bones.size(); ++i)
	{
		if (_bones[i]->boneName == boneName)
			return i;
	}

	return -1;
}

FbxAMatrix FBXLoader::GetTransform(FbxNode* node)
{
	const FbxVector4 translation = node->GetGeometricTranslation(FbxNode::eSourcePivot);
	const FbxVector4 rotation = node->GetGeometricRotation(FbxNode::eSourcePivot);
	const FbxVector4 scaling = node->GetGeometricScaling(FbxNode::eSourcePivot);
	return FbxAMatrix(translation, rotation, scaling);
}


void FBXLoader::LoadAnimationFromDir(const wstring& path)
{
	if (!fs::exists(_animDirectory))
		return;

	for (auto& mesh : _meshes)
		if (mesh.hasAnimation == false)
			mesh.hasAnimation = true;

	for (const fs::directory_entry& entry : fs::directory_iterator(_animDirectory))
	{
		shared_ptr<FBXLoader> loader = make_shared<FBXLoader>();
		loader->LoadFbx(entry.path());

		for (const auto& clip : loader.get()->GetAnimClip())
		{
			_animClips.push_back(clip);
		}
	}

}

///////////////////////////SaveFile///////////////////////////

void FBXLoader::SaveMesh(const wstring& path)
{
	FileWriter* w = new FileWriter();


	wstring meshFilePath = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".mesh";

	w->Open(meshFilePath);

	w->UInt(static_cast<UINT>(_meshes.size()));

	for (const auto& mesh : _meshes)
	{
		w->String(ws2s(mesh.name));
		w->Bool(mesh.hasAnimation);

		w->UInt(static_cast<UINT>(mesh.boneWeights.size()));
		for (const auto& boneWeight : mesh.boneWeights)
		{
			w->UInt(static_cast<UINT>(boneWeight.boneWeights.size()));
			for (const auto& p : boneWeight.boneWeights)
			{
				w->Int32(p.first);
				w->Double(p.second);
			}
		}

		w->UInt(static_cast<UINT>(mesh.indices.size()));
		for (const auto& index : mesh.indices)
		{
			w->UInt(static_cast<UINT>(index.size()));
			for (const auto& i : index)
			{
				w->UInt32(i);
			}
		}

		w->UInt(static_cast<UINT>(mesh.materials.size()));
		for (const auto& material : mesh.materials)
		{
			w->String(ws2s(material.name));
			w->String(ws2s(material.diffuseTexName));
			w->String(ws2s(material.normalTexName));
			w->String(ws2s(material.specularTexName));
			w->Vector4(material.diffuse);
			w->Vector4(material.ambient);
			w->Vector4(material.specular);
		}

		w->UInt(static_cast<UINT>(mesh.vertices.size()));
		for (const auto& vertex : mesh.vertices)
		{
			w->Vector3(vertex.pos);
			w->Vector2(vertex.uv);
			w->Vector3(vertex.normal);
			w->Vector3(vertex.tangent);
			w->Vector4(vertex.weights);
			w->Vector4(vertex.indices);
		}
	}
	
	w->Close();
	SAFE_DELETE(w);
}

void FBXLoader::SaveBone(const wstring& path)
{
	FileWriter* w = new FileWriter();

	wstring boneFilePath = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".bones";

	w->Open(boneFilePath);


	w->UInt(static_cast<UINT>(_bones.size()));
	for (const auto& bone : _bones)
	{
		string boneName = ws2s(bone->boneName);

		string deleteString = "mixamorig:";
		boneName.replace(boneName.find(deleteString), deleteString.length(), "");

		w->String(boneName);
		w->Int32(bone->parentIndex);

		w->Double(static_cast<double>(bone->matOffset[0][0]));
		w->Double(static_cast<double>(bone->matOffset[0][1]));
		w->Double(static_cast<double>(bone->matOffset[0][2]));
		w->Double(static_cast<double>(bone->matOffset[0][3]));

		w->Double(static_cast<double>(bone->matOffset[1][0]));
		w->Double(static_cast<double>(bone->matOffset[1][1]));
		w->Double(static_cast<double>(bone->matOffset[1][2]));
		w->Double(static_cast<double>(bone->matOffset[1][3]));

		w->Double(static_cast<double>(bone->matOffset[2][0]));
		w->Double(static_cast<double>(bone->matOffset[2][1]));
		w->Double(static_cast<double>(bone->matOffset[2][2]));
		w->Double(static_cast<double>(bone->matOffset[2][3]));

		w->Double(static_cast<double>(bone->matOffset[3][0]));
		w->Double(static_cast<double>(bone->matOffset[3][1]));
		w->Double(static_cast<double>(bone->matOffset[3][2]));
		w->Double(static_cast<double>(bone->matOffset[3][3]));
	}
	w->Close();
	SAFE_DELETE(w);

}

void FBXLoader::SaveAnimation(const wstring& path)
{

	if (!fs::exists(_animDirectory))
		return;

	FileWriter* w = new FileWriter();

	wstring animFilePath = fs::path(path).parent_path().wstring() + L"\\" + fs::path(path).filename().stem().wstring() + L".animations";

	w->Open(animFilePath);

	w->UInt(static_cast<UINT>(_animClips.size()));


	wstring text = L"";


	for (const auto& animClip : _animClips)
	{
		w->String(ws2s(animClip->name));

		w->Int64(animClip->startTime.Get());
		w->Int64(animClip->endTime.Get());

		w->Int(animClip->mode);
	}



	for (const auto& animClip : _animClips)
	{
		for (const auto& keyFrame : animClip->keyFrames)
		{
			w->UInt(static_cast<UINT>(keyFrame.size()));
			for (const auto& key : keyFrame)
			{

				w->Double(key.time);
				w->Double(static_cast<double>(key.matTransform[0][0]));
				w->Double(static_cast<double>(key.matTransform[0][1]));
				w->Double(static_cast<double>(key.matTransform[0][2]));
				w->Double(static_cast<double>(key.matTransform[0][3]));

				w->Double(static_cast<double>(key.matTransform[1][0]));
				w->Double(static_cast<double>(key.matTransform[1][1]));
				w->Double(static_cast<double>(key.matTransform[1][2]));
				w->Double(static_cast<double>(key.matTransform[1][3]));

				w->Double(static_cast<double>(key.matTransform[2][0]));
				w->Double(static_cast<double>(key.matTransform[2][1]));
				w->Double(static_cast<double>(key.matTransform[2][2]));
				w->Double(static_cast<double>(key.matTransform[2][3]));

				w->Double(static_cast<double>(key.matTransform[3][0]));
				w->Double(static_cast<double>(key.matTransform[3][1]));
				w->Double(static_cast<double>(key.matTransform[3][2]));
				w->Double(static_cast<double>(key.matTransform[3][3]));
			}
		}
	}


	w->Close();

	SAFE_DELETE(w);
}

///////////////////////////LoadFile///////////////////////////

void FBXLoader::LoadMeshFromFile(const wstring& path)
{
	FileReader* r = new FileReader();
	r->Open(path);

	UINT count = 0;

	count = r->UInt();


	_meshes.resize(count);


	for (UINT i = 0; i < _meshes.size(); i++)
	{
		_meshes[i].name = s2ws(r->String());
		_meshes[i].hasAnimation = r->Bool();


		count = r->UInt();
		_meshes[i].boneWeights.resize(count);


		for (UINT j = 0; j < _meshes[i].boneWeights.size(); j++)
		{
			count = r->UInt();
			_meshes[i].boneWeights[j].boneWeights.resize(count);

			for (UINT k = 0; k < _meshes[i].boneWeights[j].boneWeights.size(); k++)
			{
				_meshes[i].boneWeights[j].boneWeights[k].first = r->Int32();
				_meshes[i].boneWeights[j].boneWeights[k].second = r->Double();
			}

		}


		count = r->UInt();
		_meshes[i].indices.resize(count);

		for (UINT j = 0; j < _meshes[i].indices.size(); j++)
		{
			count = r->UInt();
			_meshes[i].indices[j].resize(count);
			for (UINT k = 0; k < _meshes[i].indices[j].size(); k++)
			{
				_meshes[i].indices[j][k] = r->UInt32();
			}
		}

		count = r->UInt();
		_meshes[i].materials.resize(count);
		for (UINT j = 0; j < _meshes[i].materials.size(); j++)
		{
			_meshes[i].materials[j].name = s2ws(r->String());
			_meshes[i].materials[j].diffuseTexName = s2ws(r->String());
			_meshes[i].materials[j].normalTexName = s2ws(r->String());
			_meshes[i].materials[j].specularTexName = s2ws(r->String());
			_meshes[i].materials[j].diffuse = r->Vector4();
			_meshes[i].materials[j].ambient = r->Vector4();
			_meshes[i].materials[j].specular = r->Vector4();
		}

		count = r->UInt();
		_meshes[i].vertices.resize(count);
		for (UINT32 j = 0; j < _meshes[i].vertices.size(); j++)
		{
			_meshes[i].vertices[j].pos = r->Vector3();
			_meshes[i].vertices[j].uv = r->Vector2();
			_meshes[i].vertices[j].normal = r->Vector3();
			_meshes[i].vertices[j].tangent = r->Vector3();
			_meshes[i].vertices[j].weights = r->Vector4();
			_meshes[i].vertices[j].indices = r->Vector4();
		}

	}
	r->Close();
	SAFE_DELETE(r);

}

void FBXLoader::LoadBoneFromFile(const wstring& path)
{
	FileReader* r = new FileReader();
	r->Open(path);

	UINT count = 0;

	count = r->UInt();

	for (UINT i = 0; i < count; i++)
	{
		shared_ptr<FbxBoneInfo> bone = make_shared<FbxBoneInfo>();


		string boneName = "";
		boneName = r->String();
		bone->boneName = s2ws(boneName);
		bone->parentIndex = r->Int32();

		bone->matOffset[0][0] = r->Double();
		bone->matOffset[0][1] = r->Double();
		bone->matOffset[0][2] = r->Double();
		bone->matOffset[0][3] = r->Double();

		bone->matOffset[1][0] = r->Double();
		bone->matOffset[1][1] = r->Double();
		bone->matOffset[1][2] = r->Double();
		bone->matOffset[1][3] = r->Double();

		bone->matOffset[2][0] = r->Double();
		bone->matOffset[2][1] = r->Double();
		bone->matOffset[2][2] = r->Double();
		bone->matOffset[2][3] = r->Double();

		bone->matOffset[3][0] = r->Double();
		bone->matOffset[3][1] = r->Double();
		bone->matOffset[3][2] = r->Double();
		bone->matOffset[3][3] = r->Double();

		_bones.push_back(bone);
	}


	r->Close();
	SAFE_DELETE(r);

}

void FBXLoader::LoadAnimFromFile(const wstring& path)
{
	FileReader* r = new FileReader();
	r->Open(path);

	UINT count = 0;


	count = r->UInt();

	for (UINT i = 0; i < count; i++)
	{
		shared_ptr<FbxAnimClipInfo> clip = make_shared<FbxAnimClipInfo>();
		string str = r->String();

		clip->name = s2ws(str);

		int64 time = 0;
		time = r->Int64();
		clip->startTime.Set(time);

		time = r->Int64();
		clip->endTime.Set(time);

		int c = 0;
		c = r->Int();
		clip->mode = fbxsdk::FbxTime::EMode(c);

		clip->keyFrames.resize(_bones.size());
		_animClips.push_back(clip);


	}


	for (UINT i = 0; i < _animClips.size(); i++)
	{

		for (UINT j = 0; j < _animClips[i]->keyFrames.size(); j++)
		{
			UINT key = r->UInt();

			for (UINT k = 0; k < key; k++)
			{
				FbxKeyFrameInfo keyFrameInfo = {};

				double temp = 0.0;
				temp = r->Double();
				keyFrameInfo.time = temp;

				temp = r->Double();
				keyFrameInfo.matTransform[0][0] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[0][1] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[0][2] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[0][3] = temp;

				temp = r->Double();
				keyFrameInfo.matTransform[1][0] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[1][1] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[1][2] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[1][3] = temp;

				temp = r->Double();
				keyFrameInfo.matTransform[2][0] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[2][1] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[2][2] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[2][3] = temp;

				temp = r->Double();
				keyFrameInfo.matTransform[3][0] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[3][1] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[3][2] = temp;
				temp = r->Double();
				keyFrameInfo.matTransform[3][3] = temp;
				_animClips[i]->keyFrames[j].push_back(keyFrameInfo);
			}

		}

	}
	r->Close();

	SAFE_DELETE(r);

}

void FBXLoader::checkIndice()
{

	wstring text = L"";

	for (const auto& b : _bones)
	{
		text += L"bone Name - " + b->boneName + L"\n";
		text += L"parentIdx - " + to_wstring(b->parentIndex) + L"\n";
		text += L"mat\n";
		text += to_wstring(b->matOffset.mData[0][0]) + L" ";
		text += to_wstring(b->matOffset.mData[0][1]) + L" ";
		text += to_wstring(b->matOffset.mData[0][2]) + L" ";
		text += to_wstring(b->matOffset.mData[0][3]) + L"\n";

		text += to_wstring(b->matOffset.mData[1][0]) + L" ";
		text += to_wstring(b->matOffset.mData[1][1]) + L" ";
		text += to_wstring(b->matOffset.mData[1][2]) + L" ";
		text += to_wstring(b->matOffset.mData[1][3]) + L"\n";

		text += to_wstring(b->matOffset.mData[2][0]) + L" ";
		text += to_wstring(b->matOffset.mData[2][1]) + L" ";
		text += to_wstring(b->matOffset.mData[2][2]) + L" ";
		text += to_wstring(b->matOffset.mData[2][3]) + L"\n";

		text += to_wstring(b->matOffset.mData[3][0]) + L" ";
		text += to_wstring(b->matOffset.mData[3][1]) + L" ";
		text += to_wstring(b->matOffset.mData[3][2]) + L" ";
		text += to_wstring(b->matOffset.mData[3][3]) + L"\n";

	}


	//OutputDebugString(text.c_str());
}
