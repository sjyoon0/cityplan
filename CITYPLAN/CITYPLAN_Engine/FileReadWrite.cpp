#include "pch.h"
#include "FileReadWrite.h"

FileWriter::FileWriter()
	: fileHandle(NULL), size(0)
{

}

FileWriter::~FileWriter()
{

}

void FileWriter::Open(wstring filePath, UINT openOption)
{
	assert(filePath.length() > 0);
	fileHandle = CreateFile
	(
		filePath.c_str()
		, GENERIC_WRITE
		, 0
		, NULL
		, openOption
		, FILE_ATTRIBUTE_NORMAL
		, NULL
	);


	bool isChecked = fileHandle != INVALID_HANDLE_VALUE;
	assert(isChecked);
}

void FileWriter::Close()
{
	if (fileHandle != NULL)
	{
		CloseHandle(fileHandle);
		fileHandle = NULL;
	}
}

void FileWriter::Bool(bool data)
{
	WriteFile(fileHandle, &data, sizeof(bool), &size, NULL);
}

void FileWriter::Word(WORD data)
{
	WriteFile(fileHandle, &data, sizeof(WORD), &size, NULL);
}

void FileWriter::Int(int data)
{
	WriteFile(fileHandle, &data, sizeof(int), &size, NULL);
}

void FileWriter::Int32(int32 data)
{
	WriteFile(fileHandle, &data, sizeof(int32), &size, NULL);
}

void FileWriter::Int64(int64 data)
{
	WriteFile(fileHandle, &data, sizeof(int64), &size, NULL);
}

void FileWriter::UInt(UINT data)
{
	WriteFile(fileHandle, &data, sizeof(UINT), &size, NULL);
}

void FileWriter::UInt32(uint32 data)
{
	WriteFile(fileHandle, &data, sizeof(uint32), &size, NULL);

}

void FileWriter::UInt64(uint64 data)
{
	WriteFile(fileHandle, &data, sizeof(uint64), &size, NULL);

}

void FileWriter::Float(float data)
{
	WriteFile(fileHandle, &data, sizeof(float), &size, NULL);
}

void FileWriter::Double(double data)
{
	WriteFile(fileHandle, &data, sizeof(double), &size, NULL);
}

void FileWriter::Vector2(const Vec2& data)
{
	WriteFile(fileHandle, &data, sizeof(Vec2), &size, NULL);
}

void FileWriter::Vector3(const Vec3& data)
{
	WriteFile(fileHandle, &data, sizeof(Vec3), &size, NULL);
}

void FileWriter::Vector4(const Vec4& data)
{
	WriteFile(fileHandle, &data, sizeof(Vec4), &size, NULL);
}

void FileWriter::BinaryMatrix(const Matrix& data)
{
	WriteFile(fileHandle, &data, sizeof(Matrix), &size, NULL);

}


void FileWriter::String(const string& data)
{
	UInt(data.size());

	const char* str = data.c_str();
	WriteFile(fileHandle, str, data.size(), &size, NULL);
}

void FileWriter::Byte(void* data, UINT dataSize)
{
	WriteFile(fileHandle, data, dataSize, &size, NULL);
}

//////////////////////////////////////////////////////////////////////////

FileReader::FileReader()
	: fileHandle(NULL), size(0)
{

}

FileReader::~FileReader()
{

}

void FileReader::Open(wstring filePath)
{
	assert(filePath.length() > 0);
	fileHandle = CreateFile
	(
		filePath.c_str()
		, GENERIC_READ
		, FILE_SHARE_READ
		, NULL
		, OPEN_EXISTING
		, FILE_ATTRIBUTE_NORMAL
		, NULL
	);


	bool isChecked = fileHandle != INVALID_HANDLE_VALUE;
	assert(isChecked);
}

void FileReader::Close()
{
	if (fileHandle != NULL)
	{
		CloseHandle(fileHandle);
		fileHandle = NULL;
	}
}

bool FileReader::Bool()
{
	bool temp = false;
	ReadFile(fileHandle, &temp, sizeof(bool), &size, NULL);

	return temp;
}

WORD FileReader::Word()
{
	WORD temp = 0;
	ReadFile(fileHandle, &temp, sizeof(WORD), &size, NULL);

	return temp;
}

int FileReader::Int()
{
	int temp = 0;
	ReadFile(fileHandle, &temp, sizeof(int), &size, NULL);

	return temp;
}

int32 FileReader::Int32()
{

	int32 temp = 0;
	ReadFile(fileHandle, &temp, sizeof(int32), &size, NULL);

	return temp;
}

int64 FileReader::Int64()
{
	int64 temp = 0;
	ReadFile(fileHandle, &temp, sizeof(int64), &size, NULL);

	return temp;
}

UINT FileReader::UInt()
{
	UINT temp = 0;
	ReadFile(fileHandle, &temp, sizeof(UINT), &size, NULL);

	return temp;
}

uint32 FileReader::UInt32()
{
	uint32 temp = 0;
	ReadFile(fileHandle, &temp, sizeof(uint32), &size, NULL);

	return temp;
}

uint64 FileReader::UInt64()
{
	uint64 temp = 0;
	ReadFile(fileHandle, &temp, sizeof(uint64), &size, NULL);

	return temp;
}

float FileReader::Float()
{
	float temp = 0.0f;
	ReadFile(fileHandle, &temp, sizeof(float), &size, NULL);

	return temp;
}

double FileReader::Double()
{
	double temp = 0.0f;
	ReadFile(fileHandle, &temp, sizeof(double), &size, NULL);

	return temp;
}

Vec2 FileReader::Vector2()
{
	float x = Float();
	float y = Float();

	return Vec2(x, y);
}

Vec3 FileReader::Vector3()
{
	float x = Float();
	float y = Float();
	float z = Float();

	return Vec3(x, y, z);
}

Vec4 FileReader::Vector4()
{
	float x = Float();
	float y = Float();
	float z = Float();
	float w = Float();

	return Vec4(x, y, z, w);
}

string FileReader::String()
{
	UINT size = Int();

	char* temp = new char[size + 1];
	ReadFile(fileHandle, temp, sizeof(char) * size, &this->size, NULL); 
	temp[size] = '\0';

	return temp;
}

void FileReader::Byte(void** data, UINT dataSize)
{
	ReadFile(fileHandle, *data, dataSize, &size, NULL);
}