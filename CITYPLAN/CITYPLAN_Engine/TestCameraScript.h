#pragma once
#include "MonoBehaviour.h"

class TestCameraScript : public MonoBehaviour
{
public:
	TestCameraScript();
	virtual ~TestCameraScript();

	virtual void Start() override;
	virtual void Update() override;
	virtual void LateUpdate() override;

private:
	float		delay = 3.0f;
	float _speed = 2000.f;
	shared_ptr<GameObject> _target;
	Vec3 _offset = Vec3(0.0f, 200.0f, -500.0f);


};
