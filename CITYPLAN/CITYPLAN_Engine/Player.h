#pragma once
#include "GameObject.h"

enum class PLAYER_TYPE
{
	NONE,
	SOLDIER,
	GANSTER,
	POLICE
};


class Player : public GameObject
{
public:
	Player();
	Player(PLAYER_TYPE type);
	virtual ~Player();
	
	PLAYER_TYPE GetPlayerType() { return _playerType; }
	void SetPlayerType(PLAYER_TYPE type) { _playerType = type; };

	void SetCharacter(const vector<shared_ptr<GameObject>>* chacter) { _chracter = chacter; }
	void AddWeapon(shared_ptr<GameObject> weapon);
	void SetMainWeapon(int index);
	shared_ptr<GameObject> GetWeaponFromName(const wstring& name);
	shared_ptr<GameObject> GetWeaponFromIndex(int index);

	int RightHandIndex() { return _rightHandIndex; }
	int LeftHandIndex() { return _leftHandIndex; }

private:
	void CreateHandIndex();

private:
	const vector<shared_ptr<GameObject>>* _chracter;
	vector<shared_ptr<GameObject>>* _weapons;

	int _mainWeaponIndex = 0;
	PLAYER_TYPE _playerType = PLAYER_TYPE::NONE;

	int _rightHandIndex = 0;
	int _leftHandIndex = 0;
};

