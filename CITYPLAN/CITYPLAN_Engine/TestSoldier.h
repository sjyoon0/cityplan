#pragma once
#include "MonoBehaviour.h"

enum ANIMNUM 
{
	Soldier,
	idle_aim_crouch,
	idle_aim_prone,
	idle_aim_stand,
	idle_crouch,
	idle_stand,
	jump_down,
	jump_loop,
	jump_up,
	reload_crouch,
	reload_prone,
	reload_stand,
	run_backward_stand,
	run_forward_stand,
	run_left_stand,
	run_right_stand,
	shoot_crouch,
	shoot_prone,
	shoot_stand,
	throw_grenade_crouch,
	throw_grenade_prone,
	throw_grenade_stand,
	walking_backward_stand,
	walking_forward_stand,
	walking_left_stand,
	walking_right_stand,

};

class TestSoldier : public MonoBehaviour
{
public:
	TestSoldier();
	virtual ~TestSoldier();

	virtual void Awake() override;
	virtual void Start() override;
	virtual void Update() override;
	virtual void LateUpdate() override;

private:
	float _speed = 1000.0f;
	float _jump = 100.0f;

	float _jumpTime = 3.0f;

	bool isRun = false;
	bool isJump = false;
	int _pos = 0;
	Vec3 jumppostion = Vec3(0.0f, 0.0f, 0.0f);

	int _rightHandIndex = 30;
	int _leftHandIndex = 10;

	bool canMove = true;


};

