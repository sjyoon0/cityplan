#pragma once
#include "Component.h"
#include "Mesh.h"

class Material;
class StructuredBuffer;
class Mesh;



struct ClipData
{
	int32 index = 0;
	int32 currFrame = 0;
	int32 nextFrame = 0;
	float frameRatio = 0.0f;
	float updateTime = 0.0f;
};

struct TweenData
{
	float tweenTime = 1.0f;
	ClipData curr;
	ClipData next;
	TweenData()
	{
		curr.index = 0;
		next.index = -1;
	}

};

class Animator : public Component
{
public:
	Animator();
	virtual ~Animator();

public:
	void SetBones(const vector<BoneInfo>* bones) { _bones = bones; }
	void SetAnimClip(const vector<AnimClipInfo>* animClips);
	void PushData();

	int32 GetAnimCount() { return static_cast<uint32>(_animClips->size()); }
	int32 GetCurrentClipIndex() { return _tween.curr.index; }
	void Play(uint32 idx);

public:
	virtual void FinalUpdate() override;

private:
	const vector<BoneInfo>* _bones;
	const vector<AnimClipInfo>* _animClips;
	
	TweenData						_tween;


	shared_ptr<Material>			_computeMaterial;
	shared_ptr<StructuredBuffer>	_boneFinalMatrix;  // 특정 프레임의 최종 행렬
	bool							_boneFinalUpdated = false;
};
