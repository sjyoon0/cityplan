#include "pch.h"
#include "FBXAssimpLoader.h"
#include "Mesh.h"
#include "Resources.h"
#include "Shader.h"
#include "Material.h"
#include "FileReadWrite.h"


FBXAssimpLoader::FBXAssimpLoader()
{
	_importer = new Assimp::Importer();
}

FBXAssimpLoader::~FBXAssimpLoader()
{
	SAFE_DELETE(_importer);
}

void FBXAssimpLoader::LoadFbx(const wstring& path)
{

	Import(path);
	LoadMaterials(path);
	SaveMaterialData(path);


	ProcessNode(_scene->mRootNode);


	SaveMeshData(path);

	CreateTextures();
	CreateMaterials();
	//checkIndices();

}


void FBXAssimpLoader::Import(const wstring& path)
{


	_scene = _importer->ReadFile(ws2s(path)
		,
		aiProcess_CalcTangentSpace |

		aiProcess_JoinIdenticalVertices |        // 동일한 꼭지점 결합, 인덱싱 최적화

		aiProcess_ValidateDataStructure |        // 로더의 출력을 검증

		aiProcess_ImproveCacheLocality |        // 출력 정점의 캐쉬위치를 개선

		aiProcess_RemoveRedundantMaterials |    // 중복된 매터리얼 제거

		aiProcess_GenUVCoords |                    // 구형, 원통형, 상자 및 평면 매핑을 적절한 UV로 변환

		aiProcess_TransformUVCoords |            // UV 변환 처리기 (스케일링, 변환...)

		aiProcess_FindInstances |                // 인스턴스된 매쉬를 검색하여 하나의 마스터에 대한 참조로 제거

		aiProcess_LimitBoneWeights |            // 정점당 뼈의 가중치를 최대 4개로 제한

		aiProcess_OptimizeMeshes |                // 가능한 경우 작은 매쉬를 조인

		aiProcess_GenSmoothNormals |            // 부드러운 노말벡터(법선벡터) 생성

		aiProcess_SplitLargeMeshes |            // 거대한 하나의 매쉬를 하위매쉬들로 분활(나눔)

		aiProcess_Triangulate |                    // 3개 이상의 모서리를 가진 다각형 면을 삼각형으로 만듬(나눔)

		aiProcess_ConvertToLeftHanded |            // D3D의 왼손좌표계로 변환

		aiProcess_SortByPType);


	assert(_scene != NULL);

	_materialDirectory = L"..\\Resources\\Material";

	_textureDirectory = L"..\\Resources\\Texture\\MapTexture";

	_meshDirectory = L"..\\Resources\\Mesh";

	

}

void FBXAssimpLoader::LoadMaterials(const wstring& path)
{

	wstring matFile = _materialDirectory + L"\\" + fs::path(path).stem().wstring() + L".materials";

	if (fs::exists(fs::path(matFile)))
	{
		LoadMaterialsFromFile(matFile);
	}
	else
	{
		for (uint64 i = 0; i < _scene->mNumMaterials; ++i)
		{
			aiMaterial* srcMaterial = _scene->mMaterials[i];
			shared_ptr<AssimpMaterialInfo> materialInfo = make_shared<AssimpMaterialInfo>();

			materialInfo->name = s2ws(srcMaterial->GetName().C_Str());

			aiColor3D color;

			srcMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color);
			materialInfo->diffuse = Vec4(color.r, color.g, color.b, 1.0f);

			srcMaterial->Get(AI_MATKEY_COLOR_SPECULAR, color);
			materialInfo->specular = Vec4(color.r, color.g, color.b, 1.0f);

			srcMaterial->Get(AI_MATKEY_COLOR_AMBIENT, color);
			materialInfo->ambient = Vec4(color.r, color.g, color.b, 1.0f);

			srcMaterial->Get(AI_MATKEY_SHININESS, materialInfo->specular.z);


			aiString fileName;

			srcMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &fileName);
			
			materialInfo->diffuseTexName = s2ws(fileName.C_Str());


			srcMaterial->GetTexture(aiTextureType_SPECULAR, 0, &fileName);
			materialInfo->specularTexName = s2ws(fileName.C_Str());


			srcMaterial->GetTexture(aiTextureType_NORMALS, 0, &fileName);
			materialInfo->normalTexName = s2ws(fileName.C_Str());


			_materials.push_back(materialInfo);

		}
	}



}

AssimpMeshInfo FBXAssimpLoader::LoadMesh(aiMesh* mesh)
{
	AssimpMeshInfo meshInfo;

	aiMaterial* material = _scene->mMaterials[mesh->mMaterialIndex];
	meshInfo.materialName = s2ws(material->GetName().C_Str());
	
	// Walk through each of the mesh's vertices
	for (UINT i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		
		vertex.pos.x = mesh->mVertices[i].x;
		vertex.pos.y = mesh->mVertices[i].y;
		vertex.pos.z = mesh->mVertices[i].z;

		if (mesh->mTextureCoords[0])
		{
			vertex.uv.x = (float)mesh->mTextureCoords[0][i].x;
			vertex.uv.y = (float)mesh->mTextureCoords[0][i].y;
		}

		if (mesh->mNormals)
		{
			vertex.normal.x = mesh->mNormals[i].x;
			vertex.normal.y = mesh->mNormals[i].y;
			vertex.normal.z = mesh->mNormals[i].z;
		}

		if (mesh->mTangents)
		{
			vertex.tangent.x = mesh->mTangents[i].x;
			vertex.tangent.y = mesh->mTangents[i].y;
			vertex.tangent.z = mesh->mTangents[i].z;
		}

		meshInfo.vertices.push_back(vertex);

	}

	for (UINT i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace& face = mesh->mFaces[i];

		for (UINT j = 0; j < face.mNumIndices; j++)
			meshInfo.indices.push_back(face.mIndices[j]);
	}

	return meshInfo;

}

void FBXAssimpLoader::ProcessNode(aiNode* node)
{
	for (UINT i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = _scene->mMeshes[node->mMeshes[i]];
		
		_meshes.push_back(LoadMesh(mesh));
		_meshes[i].name = s2ws(node->mName.C_Str())+L"_" + to_wstring(i);
	}

	for (UINT i = 0; i < node->mNumChildren; i++)
	{
		this->ProcessNode(node->mChildren[i]);
	}


}


void FBXAssimpLoader::LoadMaterialsFromFile(const wstring& path)
{
	wstring matFile = _materialDirectory + L"\\" + fs::path(path).stem().wstring() + L".materials";


	tinyxml2::XMLDocument* document = new tinyxml2::XMLDocument();
	tinyxml2::XMLError error = document->LoadFile(ws2s(matFile).c_str());
	assert(error == tinyxml2::XML_SUCCESS);
	tinyxml2::XMLElement* root = document->FirstChildElement();
	tinyxml2::XMLElement* materialNode = root->FirstChildElement();



	do
	{
		shared_ptr<AssimpMaterialInfo> material = make_shared<AssimpMaterialInfo>();

		tinyxml2::XMLElement* node = NULL;
		node = materialNode->FirstChildElement();
		material->name = s2ws(node->GetText());

		wstring texture = L"";
		node = node->NextSiblingElement();
		texture = s2ws(node->GetText());
		if (texture.length() > 0)
			material->diffuseTexName = texture;

		node = node->NextSiblingElement();
		texture = s2ws(node->GetText());
		if (texture.length() > 0)
			material->normalTexName = texture;

		node = node->NextSiblingElement();
		texture = s2ws(node->GetText());
		if (texture.length() > 0)
			material->specularTexName = texture;

		Vec4 color;

		node = node->NextSiblingElement();
		color.x = node->FloatAttribute("R");
		color.y = node->FloatAttribute("G");
		color.z = node->FloatAttribute("B");
		color.w = node->FloatAttribute("A");
		material->diffuse = color;

		node = node->NextSiblingElement();
		color.x = node->FloatAttribute("R");
		color.y = node->FloatAttribute("G");
		color.z = node->FloatAttribute("B");
		color.w = node->FloatAttribute("A");
		material->specular = color;

		node = node->NextSiblingElement();
		color.x = node->FloatAttribute("R");
		color.y = node->FloatAttribute("G");
		color.z = node->FloatAttribute("B");
		color.w = node->FloatAttribute("A");
		material->ambient = color;

		_materials.push_back(material);

		materialNode = materialNode->NextSiblingElement();


	} while (materialNode != NULL);

}

void FBXAssimpLoader::SaveMaterialData(const wstring& path, bool isOverwrite)
{
	wstring matFile = _materialDirectory + L"\\" + fs::path(path).stem().wstring() + L".materials";

	if (isOverwrite == false)
	{
		if (fs::exists(fs::path(matFile)) == true)
			return;
	}


	if (!fs::exists(_materialDirectory))
		fs::create_directory(fs::path(_materialDirectory));


	shared_ptr<tinyxml2::XMLDocument> document = make_shared<tinyxml2::XMLDocument>();

	tinyxml2::XMLDeclaration* decl = document->NewDeclaration();
	document->LinkEndChild(decl);
	tinyxml2::XMLElement* root = document->NewElement("Materials");

	root->SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	root->SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
	document->LinkEndChild(root);



	int num = 0;
	for (const auto& mat : _materials)
	{
		tinyxml2::XMLElement* node = document->NewElement("Material");
		root->LinkEndChild(node);

		tinyxml2::XMLElement* element = NULL;

		element = document->NewElement("Name");
		element->SetText(ws2s(mat->name).c_str());
		node->LinkEndChild(element);


		wstring relativePath = mat->diffuseTexName.c_str();
		wstring filename = fs::path(relativePath).filename();
		wstring fullPath = _textureDirectory + L"\\" + filename;

		element = document->NewElement("diffuseTexName");
		element->SetText(ws2s(fullPath).c_str());
		node->LinkEndChild(element);


		relativePath = mat->normalTexName.c_str();
		filename = fs::path(relativePath).filename();
		fullPath = _textureDirectory + L"\\" + filename;

		element = document->NewElement("normalTexName");
		element->SetText(ws2s(fullPath).c_str());
		node->LinkEndChild(element);


		relativePath = mat->specularTexName.c_str();
		filename = fs::path(relativePath).filename();
		fullPath = _textureDirectory + L"\\" + filename;

		element = document->NewElement("specularTexName");
		element->SetText(ws2s(fullPath).c_str());
		node->LinkEndChild(element);


		element = document->NewElement("DIFFUSE");
		element->SetAttribute("R", mat->diffuse.x);
		element->SetAttribute("G", mat->diffuse.y);
		element->SetAttribute("B", mat->diffuse.z);
		element->SetAttribute("A", mat->diffuse.w);
		node->LinkEndChild(element);

		element = document->NewElement("SPECULAR");
		element->SetAttribute("R", mat->specular.x);
		element->SetAttribute("G", mat->specular.y);
		element->SetAttribute("B", mat->specular.z);
		element->SetAttribute("A", mat->specular.w);
		node->LinkEndChild(element);


		element = document->NewElement("AMBIENT");
		element->SetAttribute("R", mat->ambient.x);
		element->SetAttribute("G", mat->ambient.y);
		element->SetAttribute("B", mat->ambient.z);
		element->SetAttribute("A", mat->ambient.w);
		node->LinkEndChild(element);

		num++;

	}
	document->SaveFile(ws2s(matFile).c_str());

}

void FBXAssimpLoader::SaveMeshData(const wstring& path, bool isOverwrite)
{
	wstring meshFile = _meshDirectory + L"\\" + fs::path(path).stem().wstring() + L".mesh";

	if (isOverwrite == false)
	{
		if (fs::exists(fs::path(meshFile)) == true)
			return;
	}


	if (!fs::exists(_meshDirectory))
		fs::create_directory(fs::path(_meshDirectory));

	
	FileWriter* w = new FileWriter();

	w->Open(meshFile);

	w->UInt(static_cast<UINT>(_meshes.size()));

	for (const auto& mesh : _meshes)
	{

		string str = ws2s(mesh.name);
		w->String(str);
		str.clear();
		str = ws2s(mesh.materialName);

		w->UInt(static_cast<UINT>(mesh.vertices.size()));
		for (const auto& vertex : mesh.vertices)
		{
			
			w->Vector3(vertex.pos);
			w->Vector2(vertex.uv);
			w->Vector3(vertex.normal);
			w->Vector3(vertex.tangent);


		}

		w->UInt(static_cast<UINT>(mesh.indices.size()));
		for (const auto& index : mesh.indices)
		{
			w->UInt32(index);
		}

	}

}

void FBXAssimpLoader::CreateTextures()
{

	for (size_t i = 0; i < _materials.size(); i++)
	{
		{
			// DiffuseTexture
			{
				wstring relativePath = _materials[i]->diffuseTexName.c_str();
				wstring filename = fs::path(relativePath).filename();
				wstring fullPath = _textureDirectory + L"\\" + filename;


				if (filename.empty() == false)
				{
					if (GET_SINGLE(Resources)->Get<Texture>(filename) == nullptr)
					{
						GET_SINGLE(Resources)->Load<Texture>(filename, fullPath);
					}

				}
			}

			// NormalTexture
			{
				wstring relativePath = _materials[i]->normalTexName.c_str();
				wstring filename = fs::path(relativePath).filename();
				wstring fullPath = _textureDirectory + L"\\" + filename;
				if (filename.empty() == false)
				{
					if (GET_SINGLE(Resources)->Get<Texture>(filename) == nullptr)
						GET_SINGLE(Resources)->Load<Texture>(filename, fullPath);

				}
			}

			// SpecularTexture
			{
				wstring relativePath = _materials[i]->specularTexName.c_str();
				wstring filename = fs::path(relativePath).filename();
				wstring fullPath = _textureDirectory + L"\\" + filename;
				if (filename.empty() == false)
				{
					if (GET_SINGLE(Resources)->Get<Texture>(filename) == nullptr)
						GET_SINGLE(Resources)->Load<Texture>(filename, fullPath);

				}
			}
		}
	}

}

void FBXAssimpLoader::CreateMaterials()
{
	for (size_t j = 0; j < _materials.size(); j++)
	{
		shared_ptr<Material> material = make_shared<Material>();
		wstring key = _materials[j]->name;
		material->SetName(key);

		material->SetShader(GET_SINGLE(Resources)->Get<Shader>(L"Deferred"));

		{
			wstring diffuseName = _materials[j]->diffuseTexName.c_str();
			wstring filename = fs::path(diffuseName).filename();
			wstring key = filename;
			shared_ptr<Texture> diffuseTexture = GET_SINGLE(Resources)->Get<Texture>(key);

			if (diffuseTexture)
				material->SetTexture(0, diffuseTexture);
		}

		{
			wstring normalName = _materials[j]->normalTexName.c_str();
			wstring filename = fs::path(normalName).filename();
			wstring key = filename;
			shared_ptr<Texture> normalTexture = GET_SINGLE(Resources)->Get<Texture>(key);
			if (normalTexture)
				material->SetTexture(1, normalTexture);
		}

		{
			wstring specularName = _materials[j]->specularTexName.c_str();
			wstring filename = fs::path(specularName).filename();
			wstring key = filename;
			shared_ptr<Texture> specularTexture = GET_SINGLE(Resources)->Get<Texture>(key);
			if (specularTexture)
				material->SetTexture(2, specularTexture);
		}

		if (GET_SINGLE(Resources)->Get<Material>(material->GetName()) == nullptr)
			GET_SINGLE(Resources)->Add<Material>(material->GetName(), material);
		else
			material = GET_SINGLE(Resources)->Get<Material>(material->GetName());
	}
}

void FBXAssimpLoader::checkIndices()
{


	for (const auto& mesh : _meshes)
	{
		wstring text = L"";


		for (const auto& i : mesh.vertices)
		{
			{

				text += L"pos - ";
				text += to_wstring(i.pos.x) + L", ";
				text += to_wstring(i.pos.y) + L", ";
				text += to_wstring(i.pos.z) + L"\n";

				text += L"uv - ";
				text += to_wstring(i.uv.x) + L",";
				text += to_wstring(i.uv.y) + L"\n";

				text += L"normal - ";
				text += to_wstring(i.normal.x) + L", ";
				text += to_wstring(i.normal.y) + L", ";
				text += to_wstring(i.normal.z) + L"\n";


				text += L"tangent - ";
				text += to_wstring(i.tangent.x) + L", ";
				text += to_wstring(i.tangent.y) + L", ";
				text += to_wstring(i.tangent.z) + L"\n";

			}
		}
		OutputDebugString(text.c_str());
	}





}
