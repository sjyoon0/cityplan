#include "pch.h"
#include "TestCameraScript.h"
#include "Transform.h"
#include "Camera.h"
#include "Input.h"
#include "Timer.h"
#include "SceneManager.h"
#include "GameObject.h"
#include "Scene.h"

TestCameraScript::TestCameraScript()
{


	
	

}

TestCameraScript::~TestCameraScript()
{
}

void TestCameraScript::Start()
{
	auto& gameObjects = GET_SINGLE(SceneManager)->GetActiveScene()->GetGameObjects();


	for (auto& gameObject : gameObjects)
	{
		if (gameObject->GetName() == L"player")
		{
			_target = gameObject;			
		}		
	}
}

void TestCameraScript::Update()
{
	Vec3 fixedPosition = Vec3(_target->GetTransform()->GetLocalPosition().x + _offset.x,
		_target->GetTransform()->GetLocalPosition().y+_offset.y,
		_target->GetTransform()->GetLocalPosition().z+_offset.z);


	Vec3 position = Vec3::Lerp(position, fixedPosition, DELTA_TIME * delay);


	GetTransform()->SetLocalPosition(fixedPosition);

	/*Vec3 pos = GetTransform()->GetLocalPosition();
	std::wstring text = L"pos : " + std::to_wstring(pos.x)+L", " + std::to_wstring(pos.y) + L", " + std::to_wstring(pos.z) + L"\n";
	text +=  L"position : " + std::to_wstring(position.x) + L", " + std::to_wstring(position.y) + L", " + std::to_wstring(position.z) + L"\n";
	text += L"fixedPosition : " + std::to_wstring(fixedPosition.x) + L", " + std::to_wstring(fixedPosition.y) + L", " + std::to_wstring(fixedPosition.z) + L"\n";

	OutputDebugString(text.c_str());*/

}

void TestCameraScript::LateUpdate()
{
	

	Vec3 position = GetTransform()->GetLocalPosition();

	

	if (INPUT->GetButton(KEY_TYPE::DOWN))
	{
		Vec3 rotation = GetTransform()->GetLocalRotation();
		rotation.x += DELTA_TIME * 0.5f;
		GetTransform()->SetLocalRotation(rotation);
	}

	if (INPUT->GetButton(KEY_TYPE::UP))
	{
		Vec3 rotation = GetTransform()->GetLocalRotation();
		rotation.x -= DELTA_TIME * 0.5f;
		GetTransform()->SetLocalRotation(rotation);
	}

	if (INPUT->GetButton(KEY_TYPE::RIGHT))
	{
		Vec3 rotation = GetTransform()->GetLocalRotation();
		rotation.y += DELTA_TIME * 0.5f;
		GetTransform()->SetLocalRotation(rotation);
	}

	if (INPUT->GetButton(KEY_TYPE::LEFT))
	{
		Vec3 rotation = GetTransform()->GetLocalRotation();
		rotation.y -= DELTA_TIME * 0.5f;
		GetTransform()->SetLocalRotation(rotation);
	}


	if (INPUT->GetButtonDown(KEY_TYPE::RBUTTON))
	{
		const POINT& pos = INPUT->GetMousePos();
		GET_SINGLE(SceneManager)->Pick(pos.x, pos.y);
	}

	GetTransform()->SetLocalPosition(position);
}
