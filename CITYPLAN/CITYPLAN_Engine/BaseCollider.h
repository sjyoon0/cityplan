#pragma once
#include "Component.h"

enum class ColliderType
{
	Sphere,
	OBB,
	BOX,
};

struct OBB
{
	Vec3 pos;
	Vec3 axisX;
	Vec3 axisY;
	Vec3 axisZ;

	Vec3 extents;
};


class BaseCollider : public Component
{
public:
	BaseCollider(ColliderType colliderType);
	virtual ~BaseCollider();
	ColliderType GetColliderType() { return _colliderType; }

	virtual bool Intersects(Vec4 rayOrigin, Vec4 rayDir, OUT float& distance) = 0;
	virtual bool Intersects(OBB other) = 0;

	void SetTrigger(bool isTrigger) { _isTrigger = isTrigger; }
	bool GetTrigger() { return _isTrigger; }

	OBB GetOBB() { return _box; }


private:
	ColliderType		_colliderType = {};
	bool				_isTrigger = false;

protected:
	OBB					_box;
};