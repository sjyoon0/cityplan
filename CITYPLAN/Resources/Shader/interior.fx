#ifndef _INTERIOR_FX_
#define _INTERIOR_FX_


struct VsIn {
	float4 position : Position;
	float2 tangent  : Tangent;
};

struct PsIn {
	float4 position : SV_Position;
	float2 texCoord : TexCoord;
	float3 dir      : Dir;
};

float4x4 viewProj;
float3 camPos;
float2 pos;
float3 size;

PsIn main(VsIn In, uint VertexID: SV_VertexID) {
	PsIn Out;

	float3 tangent0 = float3(In.tangent.x, 0, In.tangent.y);
	float3 tangent1 = float3(-In.tangent.y, 0, In.tangent.x);

	float4 position = In.position;
	position.xyz *= size;

	Out.texCoord.x = dot(position.xyz, tangent0);
	Out.texCoord.y = -position.y;

	position.xz += pos;
	Out.position = mul(viewProj, position);

	float3 dir = position.xyz - camPos;
	Out.dir.x = dot(dir, tangent0);
	Out.dir.y = dir.y;
	Out.dir.z = dot(dir, tangent1);

	return Out;
}


TextureCubeArray Cube;
Texture2D Wall;
SamplerState cubeFilter;
SamplerState wallFilter;

float lightThreshold;

float3 main(PsIn In) : SV_Target{
	float2 f = frac(In.texCoord);

	// Pseudo-random
	float2 r = floor(In.texCoord) * float2(0.679570, 0.785398) + float2(0.414214, 0.732051);
	float cubeIndex = frac(r.x + r.y + r.x * r.y) * 8.0;

	// Entrance position into the room
	float4 pos = float4(f * float2(2.0, -2.0) - float2(1.0, -1.0), -1.0, cubeIndex);

	// Compute position where the ray intersects the cube
	float3 id = 1.0 / In.dir;
	float3 k = abs(id) - pos * id;
	float kMin = min(min(k.x, k.y), k.z);
	pos.xyz += kMin * In.dir;

	// Sample cubemap at this position
	float4 room = Cube.Sample(cubeFilter, pos);

	// Vary the light somewhat between different windows
	float light = room.a * (1.0 + frac(5.2954 * cubeIndex));

	// Turn lights off in some windows, just leave some ambient
	room.rgb *= (frac(cubeIndex) < lightThreshold) ? light : 0.14;

	// Sample wall texture
	float4 wall = Wall.Sample(wallFilter, In.texCoord);

	// It's dark outside
	wall.rgb *= 0.2;

	// Bring out more detail from the windows
	wall.a = saturate(wall.a + 0.4);

	// Blend between wall and interior
	return lerp(room, wall.rgb, wall.a);
}


#endif