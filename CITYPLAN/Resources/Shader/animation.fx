#ifndef _ANIMATION_FX_
#define _ANIMATION_FX_

#include "params.fx"
#include "utils.fx"
#include "math.fx"

struct AnimFrameParams
{
    float4 scale;
    float4 rotation;
    float4 translation;
};

StructuredBuffer<AnimFrameParams>   g_Next_frame : register(t6);
StructuredBuffer<AnimFrameParams>   g_Curr_frame : register(t8);
StructuredBuffer<matrix>            g_offset : register(t9);
RWStructuredBuffer<matrix>          g_final : register(u0);

// ComputeAnimation
// g_int_0 : BoneCount
// g_float_0 : TweenTime
// g_vec4_0 : index / currFrame / nextFrame / frameRatio (Current)
// g_vec4_1 : index / currFrame / nextFrame / frameRatio (Next)


[numthreads(256, 1, 1)]
void CS_Main(uint threadIdx : SV_DispatchThreadID)
{
    if (g_int_0 <= threadIdx.x)
        return;

    int boneCount = g_int_0;
    float tweenTime = g_float_0;


    int currCurrFrame = int(g_vec4_0.y);
    int currNextFrame = int(g_vec4_0.z);
    float currFrameRatio = g_vec4_0.w;

    int nextIndex = int(g_vec4_1.x);
    int nextCurrFrame = int(g_vec4_1.y);
    int nextNextFrame = int(g_vec4_1.z);
    float nextFrameRatio = g_vec4_1.w;



    uint currIdx = (boneCount * currCurrFrame) + threadIdx.x;
    uint currNextIdx = (boneCount * currNextFrame) + threadIdx.x;

    uint nextIdx = (boneCount * nextCurrFrame) + threadIdx.x;
    uint nextNextIdx = (boneCount * nextNextFrame) + threadIdx.x;


    float4 quaternionZero = float4(0.f, 0.f, 0.f, 1.f);

    float4 currScale = lerp(g_Curr_frame[currIdx].scale, g_Curr_frame[currNextIdx].scale, currFrameRatio);
    float4 currRotation = QuaternionSlerp(g_Curr_frame[currIdx].rotation, g_Curr_frame[currNextIdx].rotation, currFrameRatio);
    float4 currTranslation = lerp(g_Curr_frame[currIdx].translation, g_Curr_frame[currNextIdx].translation, currFrameRatio);

    matrix matBone = MatrixAffineTransformation(currScale, quaternionZero, currRotation, currTranslation);


    [flatten]
    if(nextIndex >= 0)
    {
        float4 nextScale = lerp(g_Next_frame[nextIdx].scale, g_Next_frame[nextNextIdx].scale, nextFrameRatio);
        float4 nextRotation = QuaternionSlerp(g_Next_frame[nextIdx].rotation, g_Next_frame[nextNextIdx].rotation, nextFrameRatio);
        float4 nextTranslation = lerp(g_Next_frame[nextIdx].translation, g_Next_frame[nextNextIdx].translation, nextFrameRatio);

        float4 finalScale = lerp(currScale, nextScale, tweenTime);
        float4 finalRotation = QuaternionSlerp(currRotation, nextRotation, tweenTime);
        float4 finalTranslation = lerp(currTranslation, nextTranslation, tweenTime);

        matBone = MatrixAffineTransformation(finalScale, quaternionZero, finalRotation, finalTranslation);
    }




    g_final[threadIdx.x] = mul(g_offset[threadIdx.x], matBone);
}

#endif