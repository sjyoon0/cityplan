#include "pch.h"
#include "Timer.h"

void Timer::Init()
{
	::QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&Frequency));
	::QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&PrevCount)); // CPU Ŭ��
}

void Timer::Update()
{
	uint64 currentCount;
	::QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&currentCount));

	DeltaTime = (currentCount - PrevCount) / static_cast<float>(Frequency);
	PrevCount = currentCount;

	FrameCount++;
	FrameTime += DeltaTime;

	if (FrameTime > 1.f)
	{
		Fps = static_cast<uint32>(FrameCount / FrameTime);

		FrameTime = 0.f;
		FrameCount = 0;
	}
}