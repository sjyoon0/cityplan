#pragma once


class Timer
{
	DECLARE_SINGLE(Timer);

public:
	void Init();
	void Update();

	uint32 GetFps() { return Fps; }
	float GetDeltaTime() { return DeltaTime; }

private:
	uint64	Frequency = 0;
	uint64	PrevCount = 0;
	float	DeltaTime = 0.f;

private:
	uint32	FrameCount = 0;
	float	FrameTime = 0.f;
	uint32	Fps = 0;
};

